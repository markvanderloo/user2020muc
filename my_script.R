
penguins <- read.csv("penguins.csv")

penguins <- penguins[complete.cases(penguins), ]

out <- aggregate(penguins[3:4], mean, by = penguins[1:2])

write.csv(out, file="penguin_output.csv", row.names=FALSE)

print("All done with the penguins :-)")

